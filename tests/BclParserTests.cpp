#include <iostream>
#include "../framework/basecalls/data_representation/BclRepresentation.h"
#include "../framework/basecalls/parser/BclParser.h"
#include "../framework/basecalls/parser/CompressedBclParser.h"
#include "./catch2/catch.hpp"

using namespace std;

TEST_CASE("BclParserTests") {
    vector<uint16_t> lanes = {1};
    vector<uint16_t> tiles = {1101, 1102, 1103};
    BclRepresentation bclRepresentation(lanes, tiles); // 1 lane, 3 tiles
    BclParser bclParser(&bclRepresentation); // 1 lane, 3 tiles
    string filename = "../tests/mockFiles/Run/Data/Intensities/TestBaseCalls";

    SECTION("Should be able to parse single bcl file") {
        bclParser.parse(1, 1101, 1, filename);
        auto bcl = bclParser.bclRepresentation->getBcl(1, 1101, 0); // first lane and first tile

        // check if the correct number of reads were parsed
        // 1216 is encoded in the first four bytes of the bcl file (it is also equal to the size of the bcl file - 4)
        REQUIRE(bclParser.bclRepresentation->getBclSize(1, 1101) == 1216);

        // check if content is correctly parsed
        // the actual data is '918f 6c88 7b8b ...'
        REQUIRE(bcl[0] == (char)'\x91');
        REQUIRE(bcl[1] == (char)'\x8f');
        REQUIRE(bcl[2] == (char)'\x6c');
        REQUIRE(bcl[3] == (char)'\x88');
        REQUIRE(bcl[4] == (char)'\x7b');
        REQUIRE(bcl[5] == (char)'\x8b');
    }

    SECTION("Should be able to parse all bcl files of one cycle (for multiple tiles)") {
        // parse all bcl files of the first cycle
        for (int i = 0; i < 3; i++) {
            bclParser.parse(1, 1101 + i, 1, filename);
        }

        auto bcl_0_0 = bclParser.bclRepresentation->getBcl(1, 1101, 0); // first lane and first tile

        // check if content of the first tile is correctly parsed
        // the actual data is '918f 6c88 7b8b ...'

        REQUIRE(bcl_0_0[0] == (char)'\x91');
        REQUIRE(bcl_0_0[1] == (char)'\x8f');
        REQUIRE(bcl_0_0[2] == (char)'\x6c');
        REQUIRE(bcl_0_0[3] == (char)'\x88');
        REQUIRE(bcl_0_0[4] == (char)'\x7b');
        REQUIRE(bcl_0_0[5] == (char)'\x8b');

        // parse all bcl files of the first cycle
        for (int i = 0; i < 3; i++) {
            bclParser.parse(1, 1101 + i, 2, filename);
        }
        // we still pass 0 as parameter because we want to get the data to the LAST cycle!!! (1 would be the second last)
        auto bcl_0_1 = bclParser.bclRepresentation->getBcl(1,1102, 0); // first lane and SECOND tile

        // check if content of the second tile is correctly parsed
        // the actual data is '9b9b 9b90 9493 ...'
        REQUIRE(bcl_0_1[0] == (char)'\x9b');
        REQUIRE(bcl_0_1[1] == (char)'\x9b');
        REQUIRE(bcl_0_1[2] == (char)'\x9b');
        REQUIRE(bcl_0_1[3] == (char)'\x90');
        REQUIRE(bcl_0_1[4] == (char)'\x94');
        REQUIRE(bcl_0_1[5] == (char)'\x93');
    }

    SECTION("Should be able to parse multiple cycles") {
        // parse all bcl files of the first cycle
        for (int i = 0; i < 3; i++) {
            bclParser.parse(1, 1101 + i, 1, filename);
        }

        auto bcl_0_0 = bclParser.bclRepresentation->getBcl(1, 1101, 0); // first lane and first tile

        // check if content is correctly parsed
        // the actual data is '918f 6c88 7b8b ...'
        REQUIRE(bcl_0_0[0] == (char)'\x91');
        REQUIRE(bcl_0_0[1] == (char)'\x8f');
        REQUIRE(bcl_0_0[2] == (char)'\x6c');
        REQUIRE(bcl_0_0[3] == (char)'\x88');
        REQUIRE(bcl_0_0[4] == (char)'\x7b');
        REQUIRE(bcl_0_0[5] == (char)'\x8b');

        // parse all bcl files of the first cycle
        for (int i = 0; i < 3; i++) {
            bclParser.parse(1, 1101 + i, 2, filename);
        }

        // we still pass 0 as parameter because we want to get the data to the LAST cycle!!! (1 would be the second last)
        auto bcl_0_1 = bclParser.bclRepresentation->getBcl(1, 1101, 0);

        // check if content is correctly parsed
        // the actual data is '808e 528a 8889 ...'
        REQUIRE(bcl_0_1[0] == (char)'\x80');
        REQUIRE(bcl_0_1[1] == (char)'\x8e');
        REQUIRE(bcl_0_1[2] == (char)'\x52');
        REQUIRE(bcl_0_1[3] == (char)'\x8a');
        REQUIRE(bcl_0_1[4] == (char)'\x88');
        REQUIRE(bcl_0_1[5] == (char)'\x89');
    }
}