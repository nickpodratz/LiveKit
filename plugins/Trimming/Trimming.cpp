#include "Trimming.h"

using namespace std;

void Trimming::init() {
    this->permanentFragment = this->framework->createNewFragment("permanent-trimming-fragment");

    // Initialize data structure for storing approved cycles.
    for (auto lane : this->getConfigEntry<vector<uint16_t> >("lanes")) {
        for (auto tile : this->getConfigEntry<vector<uint16_t> >("tiles")) {

            auto numberOfReads = framework->getNumSequences(lane, tile);
            auto approvedCyclePerRead = this->permanentFragment->initArray<uint16_t>(to_string(lane) + to_string(tile), numberOfReads);

            // Zero-out data-structure such that a value translates to cycle 0.
            auto numberOfBytes = sizeof(uint16_t) * numberOfReads;
            memset(approvedCyclePerRead, 0, numberOfBytes);
        }
    }
}

FragmentContainer *Trimming::runCycle(FragmentContainer *inputFragments) {
    auto currentCycle = framework->getCurrentCycle();

    for (auto lane : this->getConfigEntry<vector<uint16_t> >("lanes")) {
        for (auto tile : this->getConfigEntry<vector<uint16_t> >("tiles")) {

            auto mostRecentBCL = framework->getBaseManager()->getMostRecentBcls(lane).front();
            auto approvedCyclePerRead = this->permanentFragment->getPointer<uint16_t>(to_string(lane) + to_string(tile));

            for (auto base : mostRecentBCL) {
                bool baseMatchesPattern = true;  // TODO: Check if base matches some pattern.

                if (!baseMatchesPattern) {
                    *approvedCyclePerRead = currentCycle;
                } // Otherwise, the approved cycle remains unchanged.

                approvedCyclePerRead++;
            }
        }
    }

    auto fragmentContainer = this->framework->createNewFragmentContainer();
    fragmentContainer->add(this->permanentFragment);

    return fragmentContainer;
}

void Trimming::setConfig() {
    this->registerConfigEntry<int>("lane", 1);
    this->registerConfigEntry<int>("tile", 1101);
    this->registerConfigEntry<string>("read", "100R,4B,4B,100R");
}
