#ifndef LIVEKIT_TRIMMING_H
#define LIVEKIT_TRIMMING_H

#include <iostream>
#include "../../framework/Plugin.h"
#include "../../framework/FrameworkInterface.h"
#include "../../framework/fragments/Fragment.cpp"

class Trimming : public Plugin {
public:
    FragmentContainer *runCycle(FragmentContainer *inputFragments) override;

    void init() override;

    FragmentContainer *runFullReadPostprocessing(FragmentContainer *inputFragments) override {
        (void) inputFragments; // UNUSED
        return this->framework->createNewFragmentContainer();
    };

    void finalize() override {};

    void setConfig() override;
};

extern "C" Trimming *create() {
    return new Trimming;
}

extern "C" void destroy(Trimming *plugin) {
    delete plugin;
}

#endif //LIVEKIT_TRIMMING_H
