set(DATA_REPRESENTATION_SOURCES
        ${CMAKE_CURRENT_SOURCE_DIR}/BclRepresentation.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/BclRepresentation.h
        ${CMAKE_CURRENT_SOURCE_DIR}/FilterRepresentation.h
        ${CMAKE_CURRENT_SOURCE_DIR}/FilterRepresentation.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/SequenceContainer.h
        ${CMAKE_CURRENT_SOURCE_DIR}/SequenceContainer.cpp
        PARENT_SCOPE)
