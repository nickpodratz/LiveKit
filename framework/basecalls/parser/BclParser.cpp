#include "BclParser.h"
#include "../../utils.h"

using namespace std;

void BclParser::parse(uint16_t lane, uint16_t tile, vector<char> &rawBclData) {
    // extract the number of reads
    uint32_t numReads;
    memcpy(&numReads, rawBclData.data(), 4);

    // set the position pointer to the beginning of the data block
    int position = 4;

    // TODO: Creating this vector as an intermediate-step increases runtime performance of the BclParser significantly
    vector<char> dataWithoutLength(rawBclData.begin() + position, rawBclData.end());
    this->bclRepresentation->addBcl(lane, tile, dataWithoutLength);
}

void BclParser::parse(uint16_t lane, uint16_t tile, uint16_t cycle, const string &bclPath) {
    string bclFileName = getBclFilename(lane, tile, cycle, bclPath);

    vector<char> *rawBclData;
    readBinaryFile(bclFileName, &rawBclData);
    BclParser::parse(lane, tile, *rawBclData);
    delete rawBclData;
}
