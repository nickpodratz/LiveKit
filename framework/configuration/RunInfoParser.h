#ifndef LIVEKIT_RUNINFOPARSER_H
#define LIVEKIT_RUNINFOPARSER_H

#include <boost/program_options.hpp>
#include <boost/property_tree/ptree.hpp>
#include <string>

typedef uint16_t CountType;

class RunInfoParser {
private:
    std::map<std::string, boost::any> runInfo_settings;

public:

    /**
     *
     * @return runInfo_settings A variables_map that contains all informations from the xml file
     */
    std::map<std::string, boost::any> getRunInfo() { return runInfo_settings; };

    /**
     * Parse the runInfo.xml file
     * @param runinfoFilename (Path to the XML file that is specified in the framework config)
     * @params lanes Vector of all existing lane numbers.
     */
    void parseRunInfo(std::string runinfoFilename);

    /**
    * Read a file in XML format. Results are stored as property tree.
    * @param xml_in Reference to the property tree to store the XML data.
    * @param xml_fname Name of the input file.
    * @return true on success
    */
    bool read_xml(boost::property_tree::ptree &xml_in, std::string xml_fname);

    /**
  * Convert the flowcell layout data to a plain vector of tile numbers.
  * @param surfaceCount The surface count.
  * @param swatchCount The swath count.
  * @param tileCount The tile count.
  * @param laneCount The lane count.
  * @param tileNamingConvention Specifying if tile numbers consists of four or five digits.
  * @param sectionPerLane Specifying how many section a lane has.
  * @param lanePerSection Specifying how many lanes belong to a section.
  * @return A vector of tile numbers.
  */
    inline std::vector<CountType>
    flowcell_layout_to_tile_numbers(CountType surfaceCount, CountType swathCount, CountType tileCount,
                                    int laneCount = 0, std::string tileNamingConvention = "",
                                    CountType sectionPerLane = 0, CountType lanePerSection = 0) {
        std::vector<uint16_t> tiles_vec;
        for (uint16_t surface = 1; surface <= surfaceCount; surface++)
            for (uint16_t swath = 1; swath <= swathCount; swath++)
                for (uint16_t tile = 1; tile <= tileCount; tile++)
                    if (tileNamingConvention == "FiveDigit") {
                        for (uint16_t lane = 1; lane <= laneCount; lane++) {
                            const unsigned firstSection = 1 + sectionPerLane * ((lane - 1) / lanePerSection);
                            for (unsigned section = firstSection; section < firstSection + sectionPerLane; ++section) {
                                tiles_vec.push_back(surface * 10000 + swath * 1000 + section * 100 + tile);
                            }
                        }
                    } else {
                        tiles_vec.push_back(surface * 1000 + swath * 100 + tile);
                    }
        // Remove duplicates of bgzf parsing, otherwise we have every tile as often as we have lanes
        sort(tiles_vec.begin(), tiles_vec.end());
        tiles_vec.erase(unique(tiles_vec.begin(), tiles_vec.end()), tiles_vec.end());
        return tiles_vec;
    }

    /**
    * Convert a vector of a desired data type to a string of delimited values.
    * The string will be created by streaming objects of type T to a stringstream, thus << must be defined for T.
    * @param vector The vector of values of type T
    * @param delim Character which will be used as delimiter in the resulting string [default: ',']
    * @return String with delimited values of original type T.
    */
    template<typename T>
    std::string join(std::vector<T> vector, char delim = ',');
};

#endif //LIVEKIT_RUNINFOPARSER_H
