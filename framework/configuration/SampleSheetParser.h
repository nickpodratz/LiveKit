#ifndef LIVEKIT_CSVPARSER_H
#define LIVEKIT_CSVPARSER_H

#include <vector>
#include <map>
#include <boost/any.hpp>

class SampleSheetParser {
    std::map<std::string, std::string> data;
    std::vector<std::vector<std::string>> dataList;

public:
    void parseSampleSheet(std::string filename, std::string delm = ",");

    // Function to fetch data from a CSV File
    std::map<std::string, std::string> getSampleData();

    int getHeaderRow();

    int getSampleRow(std::string sample);

    int getNumberOfSamples();

    int getSampleNumber(std::string sampleID);

    void fillConfigVector();

    std::string getBarcodeVector();

    std::vector<std::string> getSampleEntries(std::string column);
};

#endif //LIVEKIT_CSVPARSER_H
